SHA1 hash library:
lib.rs contains encrypt_sha1 function

main.rs contains 3 examples from SHA1 documentaion 
https://csrc.nist.gov/csrc/media/publications/fips/180/2/archive/2002-08-01/documents/fips180-2.pdf

Building:
rustc 1.59(or greater) compiler + relevant cargo version
Commands:
    cargo build --release
    cargo run --release
