use std::iter;

const BITS_IN_BYTE: usize = 8;

const SHA1_BLOCK_LENGTH_IN_BITS: usize = 512;
const SHA1_BITS_FOR_MESSAGE_LENGTH: usize = 64;

const SHA1_BYTES_FOR_MESSAGE_LENGTH: usize = SHA1_BITS_FOR_MESSAGE_LENGTH / BITS_IN_BYTE;
const SHA1_BLOCK_LENGTH_IN_BYTES: usize = SHA1_BLOCK_LENGTH_IN_BITS / BITS_IN_BYTE;

const SHA1_INITIAL_HASH_VALUE: [u32; 5] =
    [0x67452301, 0xefcdab89, 0x98badcfe, 0x10325476, 0xc3d2e1f0];

const SHA1_WORDS: usize = 80;

pub fn encrypt_sha1(message: &[u8]) -> [u32; 5] {
    let preprocessed_message = preprocess(message);

    let mut hash_value = SHA1_INITIAL_HASH_VALUE;
    for block in preprocessed_message.chunks_exact(SHA1_BLOCK_LENGTH_IN_BYTES) {
        let words = words(block);

        let funcs = [
            |[_, b, c, d, _]: [u32; 5]| (b & c) | (!b & d),
            |[_, b, c, d, _]: [u32; 5]| b ^ c ^ d,
            |[_, b, c, d, _]: [u32; 5]| (b & c) | (b & d) | (c & d),
            |[_, b, c, d, _]: [u32; 5]| b ^ c ^ d,
        ];
        let coefficients = [0x5a827999, 0x6ed9eba1, 0x8f1bbcdc, 0xca62c1d6];
        let mut working_vars = hash_value.clone();

        for i in 0..SHA1_WORDS {
            let case = i / 20;

            let temp = (working_vars[0].rotate_left(5))
                .wrapping_add((funcs[case])(working_vars))
                .wrapping_add(working_vars[4])
                .wrapping_add(coefficients[case])
                .wrapping_add(words[i]);

            working_vars[4] = working_vars[3];
            working_vars[3] = working_vars[2];
            working_vars[2] = working_vars[1].rotate_left(30);
            working_vars[1] = working_vars[0];
            working_vars[0] = temp;
        }

        for (hash, term) in hash_value.iter_mut().zip(working_vars.into_iter()) {
            *hash = hash.wrapping_add(term);
        }
    }

    hash_value
}

fn preprocess(message: &[u8]) -> Vec<u8> {
    let length_in_bytes = message.len();
    let length_in_bits = length_in_bytes * BITS_IN_BYTE;

    let bytes_to_append = if length_in_bytes % SHA1_BLOCK_LENGTH_IN_BYTES
        < SHA1_BLOCK_LENGTH_IN_BYTES - SHA1_BYTES_FOR_MESSAGE_LENGTH
    {
        SHA1_BLOCK_LENGTH_IN_BYTES
            - SHA1_BYTES_FOR_MESSAGE_LENGTH
            - length_in_bytes % SHA1_BLOCK_LENGTH_IN_BYTES
    } else {
        2 * SHA1_BLOCK_LENGTH_IN_BYTES
            - SHA1_BYTES_FOR_MESSAGE_LENGTH
            - length_in_bytes % SHA1_BLOCK_LENGTH_IN_BYTES
    };

    message
        .into_iter()
        .map(Clone::clone)
        .chain(iter::once(1 << (BITS_IN_BYTE - 1)))
        .chain(iter::repeat(0).take(bytes_to_append - 1))
        .chain(length_in_bits.to_be_bytes())
        .collect()
}

fn words(block: &[u8]) -> Vec<u32> {
    let mut words = Vec::with_capacity(SHA1_WORDS);
    for word in block.chunks_exact(4) {
        words.push(u32::from_be_bytes(word.try_into().unwrap()));
    }

    for i in words.len()..SHA1_WORDS {
        let new_word = (words[i - 3] ^ words[i - 8] ^ words[i - 14] ^ words[i - 16]).rotate_left(1);
        words.push(new_word);
    }

    words
}
