fn main() {
    // These examples are taken SHA1 specification APPENDIX A
    // https://csrc.nist.gov/csrc/media/publications/fips/180/2/archive/2002-08-01/documents/fips180-2.pdf

    println!("Testing \"abc\" string:");
    let hash = lab3::encrypt_sha1(b"abc");
    println!("SHA1: {hash:x?}\n");

    println!("Testing \"abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq\" string:");
    let hash = lab3::encrypt_sha1(b"abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq");
    println!("SHA1: {hash:x?}\n");

    println!("Testing string which consists of 1_000_000 \'a\' characters:");
    let sample: Vec<u8> = std::iter::repeat(b'a').take(1_000_000).collect();
    let hash = lab3::encrypt_sha1(&sample);
    println!("SHA1: {hash:x?}");
}
